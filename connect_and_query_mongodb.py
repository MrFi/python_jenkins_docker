# Imports
from pymongo import MongoClient
from json import dumps

# Function for the database connection and request with created query from clean_data_write_query.py
def connect_and_request():
    client = MongoClient(
        "mongodb+srv://default_user:Masterthesis@turingtest-bcxak.mongodb.net/test?retryWrites=true&w=majority")
    db = client['Words']
    collection = db['ClientWords']
    documents = collection.find()
    response = []

    for document in documents:
        document['_id'] = str(document['_id'])
        response.append(document)
    response = dumps(response)
    return response
