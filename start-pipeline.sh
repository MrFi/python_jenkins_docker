#!/usr/bin/env bash

docker build  \
    -t access_and_mail .


DOCKER_INPUT_DATA=$(cat <<EOF
"Hallo Frank und Max! Hier ist etwas Spannendes. Vielleicht ist Kacki drin....."
EOF
)


DOCKER_SEND_MAIL_TO=$(cat <<EOF
mail@viktortreu.de
EOF
)

docker run --rm -it \
    -e DOCKER_INPUT_DATA="$DOCKER_INPUT_DATA" -e DOCKER_SEND_MAIL_TO="$DOCKER_SEND_MAIL_TO" \
    access_and_mail #/bin/bash