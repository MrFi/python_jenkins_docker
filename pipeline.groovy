import groovy.json.JsonOutput

def jsonParse(def json) {
    return new groovy.json.JsonSlurperClassic().parseText(json)
}

node {
    stage('Prepare environment') {
        workspace = pwd()

        try {
            sh """
				echo 'workspace' ${workspace}
			"""

            // Get some code from a GitHub repository
            git url:GIT_URL, branch:GIT_BRANCH
        }
        catch (err) {
            notifySlack("Pipeline failed during Prepare environment stage.", false)
            throw err
        }
    }


    stage('Docker Build') {
        // Run the Docker build
        try {
            sh """
                cd ${PROJECT_NAME}
                docker build -t access_and_mail .
            """
        }
        catch (err) {
            notifySlack("Pipeline failed during Docker Build stage.", false)
            throw err
        }
    }


    stage('Docker Run') {
        try {
            sh """
                docker run --rm -dt \
                --name access_and_mail \
                -e DOCKER_INPUT_DATA="$DOCKER_INPUT_DATA" -e DOCKER_SEND_MAIL_TO="$DOCKER_SEND_MAIL_TO" \
                -e DOCKER_E1="$DOCKER_E1" -e DOCKER_E2="$DOCKER_E2" \
                -e DOCKER_E3="$DOCKER_E3" -e DOCKER_E4="$DOCKER_E4" \
                -e DOCKER_E5="$DOCKER_E5" -e DOCKER_CHECK_FOR_DUPLICATES="$DOCKER_CHECK_FOR_DUPLICATES"\
                access_and_mail
            """
        }
        catch (err) {
            echo 'Pipeline failed during Docker Run stage.'
            notifySlack("Pipeline failed during Docker Run stage.", false)
            throw err
        }

        notifySlack("Export Done.", true)
    }
}