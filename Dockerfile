FROM python:3.5

WORKDIR /test/

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./clean_data_write_query.py", "-u" ]